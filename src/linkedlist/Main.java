package linkedlist;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int choose;
		BookList<Book> bookList = new BookList<>();
		Book book;

		do {
			menuSelect();
			choose = sc.nextInt();

			switch (choose) {
			case 0:
				System.out.println("Thanks you");
				break;
			case 1:
				book = new Book();
				book.input();
				bookList.addLast(book);
				break;
			case 2:
				printBookToScreen(bookList);
				break;
			case 3:
				System.out.println("input code to search:");
				String code = sc.next();
				searchBookByCode(bookList, code);
				break;
			case 4:
				book = new Book();
				book.input();
				System.out.println("Position:");
				int position = sc.nextInt();
				bookList.addAtPosition(position, book);
				break;
			case 5:
				book = new Book();
				book.input();
				bookList.addFirst(book);
				break;
			case 6:
				System.out.println("You want delete book at position:");
				position = sc.nextInt();
				bookList.deleteAtPosition(position);
			default:
				break;
			}

		} while (choose != 0);
	}

	private static void menuSelect() {
		System.out.println("-------------------------------------");
		System.out.println("Book Management");
		System.out.println("-------------------------------------");
		System.out.println("1. Add At Last Book");
		System.out.println("2. Display Book");
		System.out.println("3. Search Book by code");
		System.out.println("4. Add At First Book");
		System.out.println("5. Add Book after position k");
		System.out.println("6. Delete Book at position k");
		System.out.println("0. Exit");
		System.out.println("-------------------------------------");
	}

	private static void printBookToScreen(BookList<Book> bookList) {
		for (int i = 0; i < bookList.size(); i++) {
			System.out.println(bookList.get(i).toString());
		}
	}
	
	public static void searchBookByCode(BookList<Book> bookList, String code) {
		BookList<Book> bookAfterSearch = new BookList<>();

		int j = 0;
		for (int i = 0; i < bookList.size(); i++) {
			if (bookList.get(i).getCode().indexOf(code) >= 0) {
				bookAfterSearch.addLast(bookAfterSearch.get(i));
				j = 1;
			}
		}

		System.out.println("File with key: [" + code + "]");

		printBookToScreen(bookAfterSearch);

		if (j == 0) {
			System.out.println("Dont have any File with key: [" + code + "]");
		}
	}
}

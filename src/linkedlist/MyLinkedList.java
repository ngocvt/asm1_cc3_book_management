package linkedlist;

public class MyLinkedList<T> {
	private Node<T> head = null;
	private int size = 0;

	public Node<T> createNodeFirst(T data) {
		Node<T> newNode = new Node<>(data);

		if (head == null) {
			head = newNode;
		} else {
			newNode.next = head;
			head = newNode;
		}
		size++;
		return head;
	}

	public Node<T> createNodeLast(T data) {
		Node<T> newTailNode = new Node<>(data);
		if (head == null) {
			head = newTailNode;
		} else {
			Node<T> tailListNode = head;
			while (tailListNode.next != null) {
				tailListNode = tailListNode.next;
			}
			tailListNode.next = newTailNode;
		}
		size++;
		return head;
	}

	public int size() {
		return size;
	}

	public T get(int index) {
		int k = 0;
		Node<T> p = head;
		while (p != null && k != index) {
			++k;
			p = p.next;
		}
		return p.data;
	}

	public Node<T> deleteHead() {
		if (head == null) {
			System.out.println("Book empty");
		} else {
			head = head.next;
		}
		return head;
	}

	public Node<T> deleteAtPosition(int position) {
		if (position == 0 || head == null) {
			head = deleteHead();
		} else {
			int k = 1;
			Node<T> p = head;
			while (p.next.next != null && k != position) {
				p = p.next;
				++k;
			}

			if (k != position) {
				head = deleteHead();
			} else {
				p.next = p.next.next;
			}
		}
		size--;
		return head;
	}

	public Node<T> addAtPosition(int position, T data) {
		if (position == 0 || head == null) {
			head = createNodeFirst(data);
		} else {
			int k = 1;
			Node<T> p = head;
			while (p != null && k != position) {
				p = p.next;
				++k;
			}

			if (k != position) {
				head = createNodeLast(data);
			} else {
				Node<T> temp = new Node<>(data);
				temp.next = p.next;
				p.next = temp;
			}
		}
		size++;
		return head;
	}
}

package linkedlist;

import java.util.Scanner;

public class Book {
	private int quantity, lender, value, price;
	private String code, title;
	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Book(int quantity, int lender, int value, int price, String code, String title) {
		super();
		this.quantity = quantity;
		this.lender = lender;
		this.value = value;
		this.price = price;
		this.code = code;
		this.title = title;
	}

	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getLender() {
		return lender;
	}
	public void setLender(int lender) {
		this.lender = lender;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void input() {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Book Title");
		title = sc.nextLine();
		System.out.println("Book quantity");
		quantity = sc.nextInt();
		System.out.println("Book lender");
		lender = sc.nextInt();
		System.out.println("Book price");
		price = sc.nextInt();
	}

	@Override
	public String toString() {
		return "Book [code=" + code + ", quantity=" + quantity + ", lender=" + lender + ", price=" + price + ", value="
				+ value + ", title=" + title + "]";
	}
}

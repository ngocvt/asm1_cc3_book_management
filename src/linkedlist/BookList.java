package linkedlist;

public class BookList<T> {
	MyLinkedList<Book> books = new MyLinkedList<Book>();

	public void addLast(Book book) {
		books.createNodeLast(book);
	}

	public void addFirst(Book book) {
		books.createNodeFirst(book);
	}

	public int size() {
		return books.size();
	}

	public Book get(int index) {
		return books.get(index);
	}

	public void deleteAtPosition(int position) {
		books.deleteAtPosition(position);
	}
	
	public void addAtPosition(int position, Book book) {
		books.addAtPosition(position, book);
	}
}
